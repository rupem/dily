(function() {
  'use strict';

  angular
    .module('dily')
    .component('acmeNavbar', acmeNavbar());

  /** @ngInject */
  function acmeNavbar() {
    var directive = {
      templateUrl: 'app/components/navbar/navbar.html',
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController(moment) {
      var vm = this;

      // "vm.creationDate" is available by directive option "bindToController: true"
      vm.relativeDate = moment(vm.creationDate).fromNow();
      vm.isCollapsed = '';

      vm.toggleCollapse = function() {
        if(vm.isCollapsed == 'collapse') {
          vm.isCollapsed = '';
        } else {
          vm.isCollapsed = 'collapse';
        }
      }
    }
  }

})();
