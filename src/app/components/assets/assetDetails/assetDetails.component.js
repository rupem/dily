(function() {
  'use strict';

  angular
    .module('dily')
    .component('assetDetails', {
      templateUrl: 'app/components/assets/assetDetails/assetDetails.html',
      controller: 'AssetDetailsController',
      controllerAs: 'vm',
      bindToController: true,
      bindings: {
        someBody: '='
      }
    });
})();