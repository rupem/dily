(function () {
  'use strict';

  angular.module('dily')
    .controller('AssetDetailsController', ['$scope', '$location','$resource',
      function ($scope, $location, $resource) {
        var vm = this;
        vm.asset = vm.someBody;
        console.log('[asset]' + JSON.stringify(vm.asset));
        vm.snakeToCamel = function (s) {
          if (s) {
            return s.charAt(0).toUpperCase() + (s.replace(/(\_\w)/g, function (m) {
                return ' ' + m[1].toUpperCase();
              })).slice(1);
          } else {
            return null;
          }
        };
        vm.delete = function (field) {

          var assetInfo = {};
          assetInfo.asset_id = vm.asset.asset_id;
          assetInfo.file_type = vm.asset.file_type;
          console.log('[assetInfo]' + assetInfo);
          console.log('deleting asset')
          //vm.asset[toSnakeCase(field.key)] = field.value;
          $resource('http://localhost:3005/assets/delete/:asset', {asset: '@asset'},
            {'update': {method: 'PUT'}})
            .update({
              'asset': JSON.stringify(assetInfo)
            }).$promise.then(function (data) {

            console.log("got data ");
            console.log(data);
          }, function (err) {
            toastr.error(err.statusText, JSON.stringify(err.status));
            console.log('error - ' + JSON.stringify(err));
          });

        }
        var attrs = [];
        var ignore = ['asset_path', 'tags', 'file_type', '$$hashKey', 'preview_src_link', 'asset_path', 'persons'];
        angular.forEach(vm.asset, function (value, key) {
          if (ignore.indexOf(key) == -1 && key && !key.match(/id/i)) {
            console.log(key);
            var obj = {
              key: vm.snakeToCamel(key),
              value: value
            };
            this.push(obj);
          }
        }, attrs);
        vm.attrs = attrs;
        var toSnakeCase = function (words) {
          return words.charAt(0).toLowerCase() + words.slice(1).replace(/[ ]([A-Z])/g, function (w) {
              return '_' + w[1].toLowerCase();
            })
        }
      }])
})();
