(function() {
	'use strict';
	
	angular
		.module('dily')
		.component('photo', {
				templateUrl: 'app/components/assets/photo/photo.html',
				bindings: {
					photo: '<'
				},
				controller: 'PhotoController',
				controllerAs: 'vm',
				bindToController: true	
		});
})();