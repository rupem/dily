(function () {
  'use strict';
  angular.module('dily')
    .controller('AssetAttributeController', ['$resource', 'toastr', function ($resource, toastr) {
      var vm = this;
      vm.editable = false;
      vm.edit = function (field) {
        vm.editable = !vm.editable;
      };
      vm.isEditable = function (field) {
        return vm.editable;
      };
      vm.save = function (field) {
        console.log(vm.asset);
        console.log(vm.asset[toSnakeCase(field.key)]);
        vm.asset[toSnakeCase(field.key)] = field.value;
        $resource('http://localhost:3005/assets/:asset', {asset: '@asset'},
          {'update': {method: 'PUT'}})
          .update({
            'asset': JSON.stringify(vm.asset)
          }).$promise.then(function (data) {

          console.log("got data ");
          console.log(data);
        }, function (err) {
          toastr.error(err.statusText, JSON.stringify(err.status));
          console.log('error - ' + JSON.stringify(err));
        });
        vm.editable = false;
      }

      vm.isPersonsField = function (attr) {
        return attr == "Persons";
      }
      var toSnakeCase = function (words) {
        return words.charAt(0).toLowerCase() + words.slice(1).replace(/[ ]([A-Z])/g, function (w) {
            return '_' + w[1].toLowerCase();
          })
      }

    }]);
})();
