(function(){
'use strict';
angular.module('dily')
		.component('assetAttribute', {
				bindings: {
					attribute: '=',
					asset: '<'
				},
				templateUrl: 'app/components/assets/assetAttribute/assetAttribute.html',
				controller: 'AssetAttributeController',
				controllerAs: 'vm'
		});
})();
