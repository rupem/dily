(function(){
	'use strict';
	angular.module('dily')
			.controller('TagContainerController', ['$resource', 'toastr','AWSIdentityService',
				function($resource, toastr, AWSIdentityService){
				var vm = this;
				vm.newTag;
				vm.saveButton = false;
				vm.blockRemoveUndo = true;
				vm.removed = [];
				vm.wtf={};
				vm.removeTag = function(index) {
        AWSIdentityService.setAWSConfig(AWSIdentityService.getCredentials()).then(function(identity) {
					vm.removed.push(vm.tags[index]);
					var tag = vm.tags[index];
						var params = [];
						params[0] = {};
						params[0].tag_name = tag;
						params[0].is_deleted = 1;
						params[0].asset_id = vm.assetId;
						params[0].file_type = vm.fileType;
						params[0].my_users_id = identity;
						console.log(params);
						params = JSON.stringify(params);
						$resource('http://localhost:3005/tags/:tags', {tags: '@tags'}, 
							{'update': {method: 'PUT'}})
						.update({
            'tags': params
          	}).$promise.then(function(data) {

            console.log("got data ");
            console.log(data);
            }, function(err) {
              toastr.error(err.statusText, JSON.stringify(err.status));
              console.log('error - ' + JSON.stringify(err));
            });
					vm.tags.splice(index,1);
					vm.blockRemoveUndo = false;
				});
				};
				vm.showForm = function(){
					vm.inputForm = !vm.inputForm;
					vm.saveButton = !vm.saveButton;
				};
				console.log(vm);
				vm.addTag = function() {
					if(/.*[^ ].*/.test(vm.newTag) == true && typeof vm.newTag != 'undefined'){
						vm.inputForm = !vm.inputForm;
						vm.saveButton = !vm.saveButton;
						if(!vm.tags){
							vm.tags = [];
						}
						createTag().then(function(data){
							var tag = data;
							console.log(tag);
							console.log(vm.tags);
							vm.tags.push(tag.tag_name);
							var params = [];
							params.push(tag);
							params = JSON.stringify(params);
							console.log("creating " + JSON.stringify(params));
	           $resource('http://localhost:3005/tags/:tags',
	            {
	              'tags': '@tags'
	            }).save({
	            'tags': params
	          	}).$promise.then(function(data) {
	            console.log("got data ");
	            console.log(data);
	            }, function(err) {
	              toastr.error(err.statusText, JSON.stringify(err.status));
	              console.log('error - ' + JSON.stringify(err));
	            });
							vm.newTag = "";
						});

					} else{
						toastr.error("You need to insert at least one nonempty character!");
					}
				};
				vm.cancel = function() {
					vm.saveButton = !vm.saveButton;
					vm.newTag = "";
				}

				var createTag = function() {
	        return new Promise(function(resolve, reject){
	        	AWSIdentityService.setAWSConfig(AWSIdentityService.getCredentials()).then(function(identity) {
						var tag ={};
						tag.my_users_id = identity;
						tag.tag_name = vm.newTag;
						tag.asset_id = vm.assetId;
						tag.file_type = vm.fileType;
						resolve(tag);
						})
	        });
				};

				vm.undoRemove = function() {
        AWSIdentityService.setAWSConfig(AWSIdentityService.getCredentials()).then(function(identity) {

					if(vm.blockRemoveUndo == false) {
						vm.tags.push(vm.removed[vm.removed.length - 1]);
						var tag = vm.removed[vm.removed.length - 1];
						var params = [];
						params[0] = {};
						params[0].tag_name = tag;
						params[0].is_deleted = 0;
						params[0].asset_id = vm.assetId;
						params[0].file_type = vm.fileType;
						params[0].my_users_id = identity;
						console.log(params);
						params = JSON.stringify(params);
						$resource('http://localhost:3005/tags/:tags', {tags: '@tags'}, 
							{'update': {method: 'PUT'}})
						.update({
            'tags': params
          	}).$promise.then(function(data) {

            console.log("got data ");
            console.log(data);
            }, function(err) {
              toastr.error(err.statusText, JSON.stringify(err.status));
              console.log('error - ' + JSON.stringify(err));
            });
						vm.removed.splice(vm.removed.length - 1, 1);
						if(vm.removed.length == 0){
							vm.blockRemoveUndo = true;
						}
					}
				});
				};

			}]);
})();