(function(){
'use strict';
angular.module('dily')
		.component('tagContainer', {
				bindings: {
					assetId : '<',
					fileType : '<',
					tags: '<'
				},
				templateUrl: 'app/components/assets/tagContainer/tagContainer.html',
				controller: 'TagContainerController',
				controllerAs: 'vm'
		});
})();