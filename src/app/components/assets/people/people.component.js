(function(){
'use strict';
angular.module('dily')
		.component('peopleContainer', {
				bindings: {
					persons: '<',
					assetId : '<',
					fileType : '<'
				},
				templateUrl: 'app/components/assets/people/people.html',
				controller: 'PeopleContainerController',
				controllerAs: 'vm'
		});
})();