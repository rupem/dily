(function() {
  'use strict';

  angular
    .module('dily')
    .factory('AssetObjectService', [
      function() {
        var service = {
          setCurrentObject: setCurrentObject,
          getCurrentObject: getCurrentObject
        };

        var currentObject = undefined;

        function setCurrentObject(givenObject) {
          currentObject = givenObject;
        };

        function getCurrentObject() {
          return currentObject;
        }

        return service;
      }
    ])
})();
