(function() {
	'use strict';
	angular
		.module('dily')
		.component('assetsContainer', {
				templateUrl: 'app/components/assets/assetsContainer/assetsContainer.html',
				controller: 'AssetsContainerController',
				controllerAs: 'vm',
				bindToController: true,
				bindings: {
					assets: '<'
				}
		});
})();