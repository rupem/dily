(function(){
	'use strict';

	angular.module('dily')
			.controller('AssetsContainerController',['ASSETS',
				function(assets) {
					var vm = this;
					vm.assets = assets;
			}])
})();