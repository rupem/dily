(function() {
  'use strict';

  angular
    .module('dily')
    .controller('UserMenuController', [
      function() {
        var vm = this;
        vm.$onInit = function() {
          vm.username = 'Iliescu';
        }
      }
    ])
})();
