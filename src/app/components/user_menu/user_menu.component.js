(function() {
  'use strict';

  angular
    .module('dily')
    .component('userMenu', {
      templateUrl: 'app/components/user_menu/user_menu.html',
      controller: 'UserMenuController',
      controllerAs: 'vm',
      bindToController: true
    })
})();
