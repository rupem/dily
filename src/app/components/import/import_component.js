(function() {
  'use strict';

  angular
    .module('dily')
    .component('import', {
      templateUrl: 'app/components/import/import2.html',
      controller: 'ImportController',
      controllerAs: 'vm',
      bindToController: true
    })
})();
