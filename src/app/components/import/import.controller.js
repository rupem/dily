//inject angular file upload directives and services.
(function () {
  'use strict';
  angular.module('dily').controller('ImportController', ['Upload', '$timeout', function (Upload, $timeout) {
    var vm = this;
    vm.temporaryArray = []
    vm.showSelected = false;
    vm.editClicked = true;
    vm.myArray = [];
    vm.checkboxes = [];
    vm.selectFiles = function (files) {
      for (var i = 0; i < files.length; i++) {
        vm.myArray.push(files[i]);
        vm.checkboxes.push(true);
      }
    };

    vm.checked = false;
    vm.openYoutube = function() {
      if(vm.checked == false){
        vm.checked = true;
      }
      else {
        vm.checked = false;
      }
      console.log(vm.checked);
    }

    vm.uploadFiles = function () {
      vm.files = []
      for (var i = 0; i < vm.myArray.length; i++)
        if (vm.checkboxes[i] == true)
          vm.files.push(vm.myArray[i]);
      if (vm.files && vm.files.length) {
        Upload.upload({
          url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
          data: {
            files: vm.files
          }
        }).then(function (response) {
          $timeout(function () {
            vm.result = response.data;
          });
        }, function (response) {
          if (response.status > 0) {
            vm.errorMsg = response.status + ': ' + response.data;
          }
        }, function (evt) {
          vm.progress =
            Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });

      }
      console.log(response.statusText);
    };
    vm.addToMyArray = function () {
      for (var i = 0; i < vm.checkboxes.length; i++) console.log("am un :" + vm.checkboxes[i] + i);
      console.log("Adaug in my array");
      vm.myArray = vm.temporaryArray;
    };
    vm.cancelUpload = function () {
      vm.myArray = [];
      vm.checkboxes = [];
    }

    vm.importViews = {};

    vm.changeImportView = function(view) {
      for(var key in vm.importViews) {
        vm.importViews[key] = undefined;
      }

      vm.importViews[view] = 'force-opacity';
    }
  }]);
})();
