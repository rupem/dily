(function() {
  'use strict';

  angular
    .module('dily')
    .component('familyTree', {
      templateUrl: 'app/components/family_tree/family_tree.html',
      controller: 'FamilyTreeController',
      controllerAs: 'vm',
      bindToController: true
    })
})();
