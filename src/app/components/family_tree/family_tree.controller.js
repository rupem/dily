(function () {
  'use strict';

  angular
    .module('dily')
    .controller('FamilyTreeController', ['$resource', 'toastr', 'AWSIdentityService',
      function ($resource, toastr, AWSIdentityService) {
        var vm = this;
        var rootNodeNickname = 'Taticut';
        vm.currentNode = {};

        vm.$onInit = function () {
          /*  $resource('http://localhost:3005/familyTree').query().$promise.then(function(nodes) {
           vm.nodes = nodes;
           vm.headers = getHeaders(nodes[0]);
           });
           */
          getData();
        };


        function getHeaders(obj) {
          var headers = [];

          for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
              headers.push(key);
            }
          }

          return headers;
        }

        vm.getPathToRoot = function (rootNickname) {
          $resource('http://localhost:3005/familyTree/getPath/:root/:source', {
            'root': '@root',
            'source': '@source'
          }).query({
            'root': rootNickname,
            'source': rootNodeNickname
          }).$promise.then(function (result) {
            var extractedResult = result[0];

            for (var key in extractedResult) {
              if (extractedResult.hasOwnProperty(key)) {
                extractedResult = extractedResult[key];
                break;
              }
            }
            alert(extractedResult);
          });
        }

        vm.getAllChildren = function (rootNickname) {
          $resource('http://localhost:3005/familyTree/children/:root', {
            'root': '@root'
          }).query({
            'root': rootNickname
          }).$promise.then(function (result) {
            var extractedResult = result[0];

            for (var key in extractedResult) {
              if (extractedResult.hasOwnProperty(key)) {
                extractedResult = extractedResult[key];
                break;
              }
            }
            alert(extractedResult);
          });
        }
        vm.deleteNode = function (userId) {
          console.log(userId);
          var node = $resource('http://localhost:3005/familyTree/:userId', {
            'userId': '@id'
          });

          node.delete({'userId': userId.person_id}, function (node) {
            getData();
          });
        }

        vm.updateFather = function (givenNode) {
          var node = $resource('http://localhost:3005/familyTree/update/:node', {
            'node': '@node'
          });

          givenNode.newNickname = vm.nickname;
          node.get({'node': JSON.stringify(givenNode)}, function (node) {
            getData();
          });
        }

        vm.createNode = function () {
          AWSIdentityService.setAWSConfig(AWSIdentityService.getCredentials()).then(function (identity) {
            vm.currentNode.my_users_id = identity;
            var node = $resource('http://localhost:3005/familyTree/add/:node/userId/asfgg', {
              'node': '@node',
            });
            console.log(vm.currentNode);
            node.get({'node': JSON.stringify(vm.currentNode)}, function (node) {
              getData();
            }, function (err) {
              console.log(err);
              toastr.error(err.data.status + err.data.message, err.data.detailed_message);
            });
          });
        }

        function getData() {
          AWSIdentityService.setAWSConfig(AWSIdentityService.getCredentials()).then(function (identity) {
            var my_users_id={};
            my_users_id.my_users_id=identity;
          $resource('http://localhost:3005/familyTree/userId/:my_users_id',{'my_users_id':JSON.stringify(my_users_id)}).query().$promise.then(function (nodes) {

            vm.nodes = nodes;
            vm.headers = getHeaders(nodes[0]);
            console.log('hector' + nodes);
          });

        })

        }

      }
    ])
})();
