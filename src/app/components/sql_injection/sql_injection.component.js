(function() {
  'use strict';

  angular
    .module('dily')
    .component('sqlInjection', {
      templateUrl: 'app/components/sql_injection/sql_injection.html',
      controller: 'SqlInjectionController',
      controllerAs: 'vm',
      bindToController: true
    })
})();
