(function() {
  'use strict';

  angular
    .module('dily')
    .controller('SqlInjectionController', ['$resource', 'toastr',
      function($resource, toastr) {
        var vm = this;

        vm.getQuestion = getQuestion;
        vm.getQuestionWithSQLi = getQuestionWithSQLi;

        function getQuestion(questionId) {
          $resource('http://localhost:3005/questions/:questionId', {'questionId': '@questionId'}).get({
            'questionId': questionId
          }).$promise.then(function(data) {
            var q = {};

            q.question = data.question[0].qa.split('<stupid_separator>')[0];
            q.answer = data.question[0].qa.split('<stupid_separator>')[1];

            vm.question = q;
          }, function(err) {
            console.log(err);
            toastr.error(JSON.stringify(err.data), JSON.stringify(err.status));
          });
        }

        function getQuestionWithSQLi(questionId) {
          $resource('http://localhost:3005/questions/unsafe/:questionId', {'questionId': '@questionId'}).get({
            'questionId': questionId
          }).$promise.then(function(data) {
            vm.question = data.question[0];
          });
        }
      }
    ])
})();
