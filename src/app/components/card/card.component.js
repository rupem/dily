(function(){
'use strict';
angular.module('dily')
		.component('card', {
				bindings: {
					card: '<'
				},
				templateUrl: 'app/components/card/card.html',
				controller: 'CardController',
				controllerAs: 'vm'
		});
})();