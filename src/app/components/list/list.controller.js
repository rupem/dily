(function() {
  'use strict';

  angular
    .module('dily')
    .controller('ListController', ['$resource', '$q', '$scope', '$timeout','toastr', 'AssetObjectService', '$state', 'ASSET_DB_CONFIGURATION','AWSIdentityService',
      function($resource, $q, $scope, $timeout, toastr, AssetObjectService, $state, ASSET_DB_CONFIGURATION, AWSIdentityService) {
        var vm = this;
        vm.queryAttributes = [];
        vm.isAdvancedSearchVisible = false;

        vm.toggleAdvancedSearch = toggleAdvancedSearch;
        vm.getDetailsFor = getDetailsFor;

        vm.advanced = {};

        vm.assetTypes = ASSET_DB_CONFIGURATION.assetTypes;

        vm.attributesOfCategories = formatCategories(ASSET_DB_CONFIGURATION.attributesOfCategories);
        
        function formatCategories(assets) {
          assets.Photo = assets.Photo.map(function(attr) {
            return formatSnakeCase(attr);
          });
          assets.Video = assets.Video.map(function(attr) {
            return formatSnakeCase(attr);
          });
          assets.Pdf = assets.Pdf.map(function(attr) {
            return formatSnakeCase(attr);
          });
          assets.Doc = assets.Doc.map(function(attr) {
            return formatSnakeCase(attr);
          });
          return assets;
        }

        function formatSnakeCase(words){
          return words.charAt(0).toUpperCase() + words.slice(1).replace(/[_]([a-z])/g, function(w) {
            return ' ' + w[1].toUpperCase();
          })
        }

        vm.toSnakeCase = function(words) {
            return words.charAt(0).toLowerCase() + words.slice(1).replace(/[ ]([A-Z])/g, function(w) {
            return '_' + w[1].toLowerCase();
          })
        }

        function getDetailsFor(asset) {
          console.log('sending ');
          console.log(asset);
          AssetObjectService.setCurrentObject(asset);
          $state.go('root.assetDetails');
        }

        function toggleAdvancedSearch() {
          vm.isAdvancedSearchVisible = !vm.isAdvancedSearchVisible;
        }

        function formatAssetList(assets){
          var asset;
          for(var ind in assets) {
            asset = assets[ind];
            delete asset.rn;
            assets[ind] = asset;
          }
          return assets;
        }

        vm.$onInit = function() {
        AWSIdentityService.setAWSConfig(AWSIdentityService.getCredentials()).then(function(identity) {

          $resource('http://localhost:3005/assets/count/:user',{'user':'@user'}).get({
            user: identity
          }).$promise.then(function(count) {
            count = count.message;
            console.log(count);
            vm.pageCountArray = new Array(Math.floor(count / 30) + 1);
            vm.pageCount = Math.floor(count / 30) + 1;
          console.log(vm.pageCountArray);
          console.log(vm.pageCount);
          });
          $resource('http://localhost:3005/assets/page/:userId/:page/:pageSize', {'userId': '@userId','page': '@page', 'pageSize': '@pageSize'}).query({
            'userId': identity,
            'page': 0,
            'pageSize': 30
          }, function(assets) {
            console.log("received");
            vm.assets = formatAssetList(assets);
            console.log(vm.assets.length);
            console.log(vm.assets);
          });
          vm.currentPage = 0;
        });
        };


        vm.isPersons = function(attr) {
          return attr == "persons";
        }

        vm.isTags = function(attr) {
          return attr == "tags";
        }

        vm.getResultsForPageAndPageSize = function(page, pageSize) {
        AWSIdentityService.setAWSConfig(AWSIdentityService.getCredentials()).then(function(identity) {
          $resource('http://localhost:3005/assets/page/:userId/:page/:pageSize', {'userId': '@userId','page': '@page', 'pageSize': '@pageSize'}).query({
            'userId': identity,
            'page': page,
            'pageSize': pageSize
          }).$promise.then(function(assets) {
            vm.assets = formatAssetList(assets);
            console.log(vm.assets.length);
            console.log(vm.assets);
          });
        });
        }

        vm.searchQuery = function() {
          var queryValidAttributes = vm.queryAttributes;
          for(var key in queryValidAttributes) {
            if(!queryValidAttributes[key]) {
              delete queryValidAttributes[key];
            }
          }
          var paramsToSend = {};
          paramsToSend.tables = queryValidAttributes;

          if(vm.isAdvancedSearchVisible == false) {
            paramsToSend.title = vm.queryString;
          } else {
            $.each(vm.advanced, function(key, val) {
              if(val == "" || val == null){
                delete vm.advanced[key];
              }else{
                vm.advanced[key] = val;
                var snakeKey = vm.toSnakeCase(key);
                paramsToSend[snakeKey] = val;
              }
            })
          }
          paramsToSend = JSON.stringify(paramsToSend);
          console.log(paramsToSend);
    
        AWSIdentityService.setAWSConfig(AWSIdentityService.getCredentials()).then(function(identity) {

           $resource('http://localhost:3005/assets/complexSearch/:userId/:params',
            {
              'userId': '@userId',
              'params': '@params'
            }).query({
            'userId': identity,
            'params': paramsToSend
          }).$promise.then(function(data) {
            console.log("got data ");
            console.log(data);
            vm.assets = data;
            var count = data.length;
            vm.pageCountArray = new Array(Math.floor(count / 30) + 1);
            vm.pageCount = Math.floor(count / 30) + 1;
            }, function(err) {
              toastr.error(err.statusText, JSON.stringify(err.status));
              console.log('error - ' + JSON.stringify(err));
            });
        });
          console.log(paramsToSend);
        }

        vm.searchCriteria = vm.attributesOfCategories.Photo;
        if(vm.searchCriteria.indexOf("Kinship Degree") == -1){
          vm.searchCriteria.push("Kinship Degree");
        }
        vm.tableHeaders = vm.attributesOfCategories.Photo;
        if(vm.tableHeaders.indexOf("Persons") == -1){
          vm.tableHeaders.push("Persons");
        }
        if(vm.tableHeaders.indexOf("Tags") == -1){
          vm.tableHeaders.push("Tags");
        }
        vm.tableToggle = false;

        function getSelectedCategories(categories) {
          var resultArray = [];

          for(var i =0; i< categories.length; i++) {
            if(categories[i].checked) {
              resultArray.push(categories[i].type);
            }
          }

          return resultArray;
        }

        vm.highlight = function(item) {
          if (item.checked == true) {
            item.checked = false;
            var index = vm.tableHeaders.indexOf(item.type);
            if(index != -1){
              vm.tableHeaders.splice(index,1);
            }
            index = vm.queryAttributes.indexOf(item.type);
            vm.queryAttributes.splice(index,1);
          }
          else {
            item.checked = true;
            if(item.type == "Persons"){
              if(vm.tableHeaders.indexOf("Persons")!=-1){
                vm.tableHeaders.push(item.type);
              }
            }
            if(item.type == "Tags"){
              vm.tableHeaders.push(item.type);
            }
            vm.queryAttributes.push(item.type);
          }
        }
      }
    ])
})();
