(function() {
  'use strict';

  angular
    .module('dily')
    .component('list', {
      templateUrl: 'app/components/list/list.html',
      controller: 'ListController',
      controllerAs: 'vm',
      bindToController: true
    })
})();
