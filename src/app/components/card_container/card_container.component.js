(function(){
'use strict';
angular.module('dily')
		.component('cardContainer', {
			templateUrl: 'app/components/card_container/card_container.html',
			controller: 'cardContainerController',
			controllerAs: 'vm'
		});
})();