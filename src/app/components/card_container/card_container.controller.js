(function(){
	'use strict';
	angular.module('dily')
			.controller('cardContainerController', function(WELCOME_PAGE_CARDS){
				var vm = this;
				vm.cards = WELCOME_PAGE_CARDS;
			});
})();