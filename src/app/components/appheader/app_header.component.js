(function() {
  'use strict';

  angular
    .module('dily')
    .component('appHeader', {
      templateUrl: 'app/components/appheader/app_header.html',
      controller: 'AppHeaderController',
      controllerAs: 'vm',
      bindToController: true
    })
})();
