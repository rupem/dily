(function() {
  'use strict';

  angular
    .module('dily')
    .component('upload', {
      templateUrl: 'app/components/upload/upload2.html',
      controller: 'UploadController',
      controllerAs: 'vm',
      bindToController: true
    })
})();
