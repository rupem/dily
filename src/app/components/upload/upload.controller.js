//inject angular file upload directives and services.
(function () {
  'use strict';
  angular.module('dily').controller('UploadController', ['Upload', '$timeout', '$scope','$resource','toastr', function (Upload, $timeout, $scope,$resource,toastr) {
    var vm = this;
    vm.temporaryArray = []
    vm.showSelected = false;
    vm.editClicked = true;
    vm.fileList = [];
    vm.checkboxes = [];
    vm.selectFiles = function (files) {
      for (var i = 0; i < files.length; i++) {
        vm.fileList.push(files[i]);
        vm.checkboxes.push(true);
      }
    };

    vm.uploadFiles = function () {
      var f = document.getElementById('file').files[0],
        r = new FileReader();

      r.onloadend = function (e) {
        var data = e.target.result;
        //send your binary data via $http or $resource or do anything else with it
        console.log('cle mai data');
        console.log(data);
        AWS.config.credentials.get(function () {
          var identityId = AWS.config.credentials.identityId;
          var s3 = new AWS.S3();

          var params = {
            'Bucket': 'dily-storage',
            'Key': identityId + '/' + f.name,
            'Body': f,
            'ContentType': f.type,
            'ACL': 'public-read'
          }
          var object = {};
          object.my_users_id = identityId;
          object.file_type = undefined;
          object.title=f.name;
          object.description = f.name;
          var objArray=[];

          s3.upload(params, function (err, data) {
            console.log('s3data: ' + JSON.stringify(data));
            // URL ici
            console.log('location: '+data.Location);
            object.asset_path = data.Location;
            object.preview_src_link=data.Location;
            objArray.push(object);
            var arrayToSend=JSON.stringify(objArray);
            var node = $resource('http://localhost:3005/addAssets/:assets',
              {
                'assets': '@assets'
              });
            node.save({
              'assets': arrayToSend
            }, function (result) {
              console.log('a terminat de inserat asset');
              console.log('got res ' + JSON.stringify(result));
            }, function (error) {
              console.log('error adding assets');
              console.log(error);
              toastr.error(error.statusText, JSON.stringify(error.status));
            });
            console.log(data.Location);
          });
        });
        $scope.$apply(function () {
          vm.fileList.push(f);
        });
      }

      r.readAsBinaryString(f);
    };

    function dataURItoBlob(dataURI) {
      var binary = atob(dataURI.split(',')[1]);
      var array = [];
      for (var i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
      }
      return new Blob([new Uint8Array(array)], {type: 'image/png'});
    }

    vm.addToMyArray = function () {
      for (var i = 0; i < vm.checkboxes.length; i++)
        console.log("am un :" + vm.checkboxes[i] + i);
      console.log("Adaug in my array");
      vm.fileList = vm.temporaryArray;
    };
    vm.cancelUpload = function () {
      vm.fileList = [];
      vm.checkboxes = [];
    }
    //vm.addOrRemoveChecked(nr)
  }]);
})();
