(function() {
  'use strict';

  angular
    .module('dily')
    .component('footerComponent', {
      templateUrl: 'app/components/footer/footer.html',
      controller: 'FooterController',
      controllerAs: 'vm',
      bindToController: true
    })
})();
