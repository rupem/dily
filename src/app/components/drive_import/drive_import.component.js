(function(){
  'use strict';
  angular.module('dily')
    .component('driveImport', {
      bindings: {
        card: '<'
      },
      templateUrl: 'app/components/drive_import/drive_import.html',
      controller: 'DriveImportController',
      controllerAs: 'vm'
    });
})();