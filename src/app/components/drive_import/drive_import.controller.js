(function() {
  'use strict';
  angular.module('dily')
    .controller('DriveImportController', ['$scope','$resource', 'NODE_API', 'toastr', '$filter','AWSIdentityService', function($scope, $resource, NODE_API, toastr, $filter, AWSIdentityService) {
      var vm = this;
      var client_id = "53044390931-oupt5pvaf3lcjgbo3b71ebg8ohbia01u.apps.googleusercontent.com";
      var scopes = ['https://www.googleapis.com/auth/drive.readonly',
        'https://www.googleapis.com/auth/drive'
      ];
      var pickerApiLoaded = false;
      var driveApi = false;
      var oauthToken;
      vm.loading = false;
      vm.loaded = false;
      vm.url = NODE_API.url;

      vm.files = JSON.parse(localStorage.getItem('files'));
      vm.success = false;
      vm.warning = false;
      vm.fail = false;

      console.log(vm.files);
      console.log(JSON.stringify(JSON.parse(localStorage.getItem('files'))));

      if(localStorage.getItem('files') != null) {
        vm.files = JSON.parse(localStorage.getItem('files'));
        console.log(vm.files);
        console.log('got files');
        vm.loaded = true;
      }

      vm.remove = function(index) {
        console.log("slicing " +index);
        console.log(vm.files);
        vm.files.splice(index,1);
        console.log(vm.files);
        localStorage.setItem('files', JSON.stringify(vm.files));
      }
      vm.sendToDb = function() {
        AWSIdentityService.setAWSConfig(AWSIdentityService.getCredentials()).then(function(identity) {
          var dataToSend = [];
          vm.count = vm.files.length;
          console.log(vm.count);
          for(var file in vm.files) {
            if(vm.files[file].broken == undefined) {
              var asset = {};
              console.log(identity);
              asset.my_users_id = identity;
              asset.title = vm.files[file].title;
              asset.posting_date = moment().format('DD-MM-YYYY hh:mm:ss');
              asset.creation_date = moment(vm.files[file].create_date).format('DD-MM-YYYY hh:mm:ss');
              console.log(asset);
              asset.file_type = vm.files[file].type;
              asset.asset_path = vm.files[file].driveLink;
              asset.preview_src_link = vm.files[file].thumbnailLink;
              dataToSend.push(asset);
            }
          }

          dataToSend = JSON.stringify(dataToSend);
          console.log(dataToSend);
          var node = $resource('http://localhost:3005/addAssets/:assets',
            {
              'assets': '@assets'
            });
          
          node.save({
            'assets': dataToSend
          }, function(result) {
            console.log('got res ' + JSON.stringify(result));
            vm.succeeded = result.changes;
            console.log("asdada");
            console.log(vm.files);
            console.log(vm.files.length);
            console.log(vm.count);
            console.log(vm.succeeded);
            if(vm.count == result.changes){
              vm.success = true;
            }else {
              vm.warning = true;
            }
          }, function(error) {
            vm.fail = true;
            console.log('error adding assets');
            console.log(error);
            //toastr.error(error.statusText, JSON.stringify(error.status));
          });


          vm.files = {};
          localStorage.removeItem('files');
          vm.loaded = false;
        });
      }
      // Use the Google API Loader script to load the google.picker script.

      function onAuthApiLoad() {

        gapi.auth.authorize({
          'client_id': client_id,
          'scope': scopes,
          'immediate': false
        },
        handleAuthResult);
      }

      function onPickerApiLoad() {
        pickerApiLoaded = true;
        createPicker();
      }

      function onDriveApiLoad() {
        driveApi = true;
      }

      vm.importDrive = function() {
        vm.loading = false;
        vm.loaded = false;
        vm.success = false;
        vm.fail = false;
        vm.warning = false;
        vm.files = {};
        selectedFiles = [];
        gapi.load('auth', {
          'callback': onAuthApiLoad
        });
        gapi.load('picker', {
          'callback': onPickerApiLoad
        });
        gapi.client.load('drive', 'v2', onDriveApiLoad);
      }

      function handleAuthResult(authResult) {
        if (authResult && !authResult.error) {
          console.log(authResult);
          oauthToken = authResult.access_token;
          createPicker();
        }
      }

      // Create and render a Picker object for searching assets.
      function createPicker() {
        if (pickerApiLoaded && oauthToken) {
          var view = new google.picker.View(google.picker.ViewId.DOCS);
          var picker = new google.picker.PickerBuilder()
            .enableFeature(google.picker.Feature.NAV_HIDDEN)
            .enableFeature(google.picker.Feature.MULTISELECT_ENABLED)
            .setOAuthToken(oauthToken)
            .addView(view)
            .addView(new google.picker.DocsUploadView())
            .setCallback(pickerCallback)
            .build();
          picker.setVisible(true);
        }
      }

      // A simple callback implementation.
      function pickerCallback(data) {
        if (data.action == google.picker.Action.PICKED) {
          vm.loading = true;
          $scope.$apply();
          console.log(data.docs);
          getData(data.docs);
        }
      }
      var countFiles;
      var selectedFiles = [];

      function loadData(fileId) {
        return new Promise(function(resolve, reject) {
          setTimeout(resolve, 3000);
          var request = gapi.client.drive.files.get({
            'fileId': fileId
          });
          request.execute(function(resp) {
            if (resp.error) {
              console.log("error");
              var file = {};
              file.title = 'Sorry, this file could not load (';
              file.broken = true;
              file.title += resp.error.message + ')';
              countFiles -= 1;
              selectedFiles.push(file);
              resolve(file);
              if (countFiles == 0) {
                vm.files = selectedFiles;
                localStorage.setItem('files', JSON.stringify(vm.files));
                vm.loaded = true;
                vm.loading = false;
                console.log("doneerr");
                console.log(JSON.parse(localStorage.files));
              }
            }else {
              console.log(resp);
              vm.resp = resp;
              countFiles -= 1;
              var file = {};
              file.id = resp.id;
              file.type = resp.fileExtension;
              file.title = resp.title;
              file.create_date = resp.createdDate;
              file.owners = resp.owners;
              file.downloadLink = resp.webContentLink;
              file.extension = file.fileExtension;
              if(file.downloadLink == undefined) {
                file.downloadHidden = true;
              }else{
                file.downloadHidden = false;
              }
              file.driveLink = resp.alternateLink;
              file.thumbnailLink = resp.thumbnailLink;
              selectedFiles.push(file);
              console.log(selectedFiles);
              console.log(countFiles);
              resolve(selectedFiles);
              if (countFiles == 0) {
                vm.files = selectedFiles;
                localStorage.setItem('files', JSON.stringify(vm.files));
                vm.loaded = true;
                vm.loading = false;
                console.log("done");
                console.log(JSON.parse(localStorage.files));
              }
            }
          });
        });
      }

      function getData(data) {
        countFiles = data.length;
        console.log(countFiles);
        var promises = [];
        for (var index in data) {
          var fileId = data[index].id;
          promises.push(loadData(fileId));
        }
        Promise.all(promises).then(function(res, err) {
          if(err){
            console.log("errerr");
          }
          $scope.$apply();
        })
      }

    }]);
})();