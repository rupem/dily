(function() {
  'use strict';

  angular
    .module('dily')
    .component('youtubeIntegration', {
      templateUrl: 'app/components/youtubeIntegration/youtubeIntegration.html',
      controller: 'YoutubeIntegrationController',
      controllerAs: 'vm',
      bindToController: true
    })
})();
