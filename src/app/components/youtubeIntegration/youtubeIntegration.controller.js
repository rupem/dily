(function() {
  'use strict';

  angular
    .module('dily')
    .controller('YoutubeIntegrationController', [ '$scope', '$resource', 'toastr', '$window', 'AWSIdentityService',
      function($scope, $resource, toastr, $window, AWSIdentityService) {
        var vm = this;


        vm.assetsArray = [];

        vm.removeElement = function(index) {

          console.log(vm.assetsArray);
          vm.assetsArray.splice(index,1);
          console.log(vm.assetsArray);
        }

        vm.sendToDatabase = function () {
          var dataArray = [];
          AWSIdentityService.setAWSConfig(AWSIdentityService.getCredentials()).then(function(identity) {
            for(var index in vm.assetsArray) {
              if(vm.assetsArray[index].broken == undefined ) {
                var asset = {};
                asset.my_users_id = identity;
                asset.title = vm.assetsArray[index].title;
                asset.posting_date = moment().format('DD-MM-YYYY hh:mm:ss');
                asset.creation_date = "";
                asset.location = "";
                asset.description = "";
                asset.file_type = ".avi";
                asset.asset_path = vm.assetsArray[index].videoLink;
                asset.preview_src_link = vm.assetsArray[index].imageLink;
                dataArray.push(asset);
              }
            }

            dataArray = JSON.stringify(dataArray);
            console.log(dataArray);

            var videoModel = $resource('http://localhost:3005/asset/video',
              {
                'assets': '@assets'
              });

            videoModel.save({
              'assets': dataArray
            }, function(result) {
              console.log('got res ' + JSON.stringify(result));
            }, function(error) {
              console.log('error adding assets');
              console.log(error);
              toastr.error(error.statusText, JSON.stringify(error.status));
            });

          });
        }

        vm.jYoutube = function( url, size ){
          if(url === null){ return ""; }

          size = (size === null) ? "big" : size;
          var vid;
          var results;

          results = url.match("[\?&]v=([^&#]*)");

          vid = ( results === null ) ? url : results[1];

          if(size == "small"){
            return "http://img.youtube.com/vi/"+vid+"/2.jpg";
          }else {
            return "http://img.youtube.com/vi/"+vid+"/0.jpg";
          }
        };


        var CLIENT_ID = "53044390931-oupt5pvaf3lcjgbo3b71ebg8ohbia01u.apps.googleusercontent.com";
        var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest"];
        var SCOPES = 'https://www.googleapis.com/auth/youtube.readonly';

        var authorizeButton = document.getElementById('authorize-button');
        var signoutButton = document.getElementById('signout-button');


        $window.handleClientLoad = function () {
          gapi.load('client:auth2', initClient);
        }


        $window.initClient = function () {
          gapi.client.init({
            discoveryDocs: DISCOVERY_DOCS,
            clientId: CLIENT_ID,
            scope: SCOPES
          }).then(function () {
            // Listen for sign-in state changes.
            gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

            // Handle the initial sign-in state.
            updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
            authorizeButton.onclick = handleAuthClick;
            signoutButton.onclick = handleSignoutClick;
          });
        }


        $window.updateSigninStatus = function (isSignedIn) {
          if (isSignedIn) {
            authorizeButton.style.display = 'none';
            signoutButton.style.display = 'block';
            getChannel();
          } else {
            authorizeButton.style.display = 'block';
            signoutButton.style.display = 'none';
          }
        }

        //Sign in the user upon button click.
        $window.handleAuthClick = function (event) {
          gapi.auth2.getAuthInstance().signIn();
        }

        //Sign out the user upon button click.
        $window.handleSignoutClick = function (event) {
          gapi.auth2.getAuthInstance().signOut();
        }


        $window.appendPre = function (message) {
          var pre = document.getElementById('content');
          var textContent = document.createTextNode(message + '\n');
          pre.appendChild(textContent);
        }

        $window.getChannel = function () {
          gapi.client.youtube.channels.list({
            'part': 'snippet,contentDetails,statistics',
            'forUsername': 'GoogleDevelopers'
          }).then(function(response) {
            var channel = response.result.items[0];

            appendPre('This channel\'s ID is ' + channel.id + '. ' +
              'Its title is \'' + channel.snippet.title + ', ' +
              'and it has ' + channel.statistics.viewCount + ' views.');

          });
        }


        //UPLOAD MODULE
        var playlistId, nextPageToken, prevPageToken;

        // After the API loads, call a function to get the uploads playlist ID.
        $window.handleAPILoaded = function () {
          requestUserUploadsPlaylistId();
        }

        // Call the Data API to retrieve the playlist ID that uniquely identifies the
        // list of videos uploaded to the currently authenticated user's channel.
        $window.requestUserUploadsPlaylistId = function () {
          var request = gapi.client.youtube.channels.list({
            mine: true,
            part: 'contentDetails'
          });
          request.execute(function(response) {
            playlistId = response.result.items[0].contentDetails.relatedPlaylists.uploads;
            requestVideoPlaylist(playlistId);
          });
        }

        // Retrieve the list of videos in the specified playlist.
        $window.requestVideoPlaylist = function (playlistId, pageToken) {
          var requestOptions = {
            playlistId: playlistId,
            part: 'snippet',
            maxResults: 10
          };
          if (pageToken) {
            requestOptions.pageToken = pageToken;
          }
          var request = gapi.client.youtube.playlistItems.list(requestOptions);
          request.execute(function(response) {
            // Only show pagination buttons if there is a pagination token for the
            // next or previous page of results.
            nextPageToken = response.result.nextPageToken;
            var nextVis = nextPageToken ? 'visible' : 'hidden';
            prevPageToken = response.result.prevPageToken
            var prevVis = prevPageToken ? 'visible' : 'hidden';

            var playlistItems = response.result.items;
            if (playlistItems) {
              $.each(playlistItems, function(index, item) {
                displayResult(item.snippet);
              });
            } else {
              displayResult("No uploaded videos to be displayed.");
            }
          });
        }

        // Create a listing for a video.
        function displayResult(videoSnippet) {
          var title = videoSnippet.title;
          var videoId = videoSnippet.resourceId.videoId;
          var videoPicture = vm.jYoutube(videoId, "small");
          var assetsObject = {};
          assetsObject.videoLink = 'http://www.youtube.com/watch?v=' + videoId;
          assetsObject.imageLink = videoPicture;
          assetsObject.title = title;
          vm.assetsArray.push(assetsObject);
          console.log(vm.assetsArray);
          $scope.$apply();
        }

        // Retrieve the next page of videos in the playlist.
        $window.nextPage = function () {
          requestVideoPlaylist(playlistId, nextPageToken);
        }

        // Retrieve the previous page of videos in the playlist.
        $window.previousPage = function () {
          requestVideoPlaylist(playlistId, prevPageToken);
        }



        var OAUTH2_CLIENT_ID = "53044390931-oupt5pvaf3lcjgbo3b71ebg8ohbia01u.apps.googleusercontent.com";
        var OAUTH2_SCOPES = [
          'https://www.googleapis.com/auth/youtube'
        ];


        // Upon loading, the Google APIs JS client automatically invokes this callback.
        $window.googleApiClientReady = function() {
          gapi.auth.init(function() {
            window.setTimeout(checkAuth, 1);
          });
        }

        $window.checkAuth = function () {
          gapi.auth.authorize({
            client_id: OAUTH2_CLIENT_ID,
            scope: OAUTH2_SCOPES,
            immediate: true
          }, handleAuthResult);
        }

        // Handle the result of a gapi.auth.authorize() call.
        $window.handleAuthResult = function (authResult) {
          if (authResult && !authResult.error) {
            $('.pre-auth').hide();
            $('.post-auth').show();
            loadAPIClientInterfaces();
          } else {
            $('#login-link').click(function() {
              gapi.auth.authorize({
                client_id: OAUTH2_CLIENT_ID,
                scope: OAUTH2_SCOPES,
                immediate: false
              }, handleAuthResult);
            });
          }
        }

        // Load the client interfaces for the Data API
        $window.loadAPIClientInterfaces = function () {
          gapi.client.load('youtube', 'v3', function() {
            handleAPILoaded();
          });
        }


      }
    ])
})();
