/**
 * Created by RobertHerscovici on 5/26/2017.
 */
(function () {
  'use strict';

  angular
    .module('dily')
    .controller('FbapiController', ['$scope', '$filter', '$resource', 'toastr', '$http','AWSIdentityService',
      function ($scope, $filter, $resource, toastr, $http, AWSIdentityService) {
        var vm = this;

        vm.existingPhotosIds = [];
        vm.data = [];
        vm.isLoading = true;
        vm.isConnected = false;
        vm.isImported = false;
        vm.promiseArray = [];
        vm.photosInfo = ['aaa'];
        vm.initFBAPI = function () {
          console.log('initializez');
          return new Promise(function (resolve) {
            window.fbAsyncInit = function () {
              FB.init({
                appId: '1058188260977870',
                status: true,
                cookie: true,
                version: 'v2.8'
              });
              resolve("Facebook loaded");
            }
          })
        }

        vm.getLoginStatus = function () {
          FB.getLoginStatus(vm.checkLoginStatusFB)
        }

        vm.checkLoginStatusFB = function (res) {
          if (res.status == "connected") {
            vm.isConnected = true;
            vm.userId = res.authResponse.userID;
            console.log('[userId]' + vm.userId);
            $scope.$apply();
          }
          vm.isLoading = false;
        }

        vm.loginIntoFB = function () {
          FB.login(function (response) {
            console.log(response);
            if (response.authResponse) {
              vm.isConnected = true;
              $scope.$apply();
              console.log('ne-am conectat');
            } else {
              console.log('User cancelled login or did not fully authorize.');
            }
          }, {scope: 'email,user_likes,user_photos'});
        }

        vm.logoutFromFB = function () {
          vm.isConnected = false;
          vm.isImported = false;
          FB.logout();
          vm.data = [];
        }

        vm.importPhotos = function () {
          vm.getExistingPhotos(vm.userId).then(FB.api('/me/photos', vm.processPhotos));
        }

        vm.processPhotos = function (response) {
          angular.forEach(response.data, vm.getIndividualInfo)
        }

        vm.getIndividualInfo = function (photoInfo) {

          vm.promiseArray.push(vm.getDataObject(photoInfo));
          Promise.all(vm.promiseArray).then(function (data) {
            vm.data = data;
            vm.isImported = true;
            $scope.$apply();
          })
        }

        vm.getDataObject = function (data) {
          return new Promise(function (resolve, reject) {
            FB.api('/' + data.id + '?fields=name,link,picture,created_time,width,height,place,id', function (response) {
              resolve(vm.getAllInfo(response));
            })
          })
        }

        vm.getAllInfo = function (data) {
          var result = {};
          result.tags = vm.gettags(data.name);
          result.description = data.name;
          result.title = data.name;
          result.link = data.link;
          result.urlImageIcon = data.picture;
          result.width = data.width;
          result.height = data.height;
          result.place = data.place;
          result.creationDate = data.created_time;
          result.id = data.id;
          if (vm.photoExists(data.id)) {
            console.log('here');
            result.isDuplicated = true;
          } else {
            result.isDuplicated = false;
          }

          // result.isDuplicated = vm.photoExists(data.id);
          console.log('[tags]' + result.isDuplicated)
          return result;

        }

        vm.gettags = function (item) {
          if (item == undefined) return undefined;
          item = JSON.stringify(item);
          var resultingTags = [];
          var descSlices = item.split('#');
          var i;
          if (descSlices.length != item.length) {
            for (i = 1; i < descSlices.length; i++) {
              var spaceIndex = descSlices.indexOf(' ');
              if (spaceIndex != -1)
                resultingTags.push(descSlices[i].substring(0, spaceIndex));
              else {
                resultingTags.push(descSlices[i].substr(0, descSlices[i].length - 1));
              }
            }
            return resultingTags;
          } else {
            return undefined;
          }
        }

        vm.removeElement = function (index) {
          vm.data.splice(index, 1);
        }

        vm.uploadDataArray = function () {
        AWSIdentityService.setAWSConfig(AWSIdentityService.getCredentials()).then(function(identity) {

          var photoArray = [];
          new Promise(function (resolve, reject) {
            console.log('[uploadData]' + 'intru');


            for (var item in vm.data) {
              var photoObj = {};
              console.log('[uploadData]' + JSON.stringify(vm.data[item]))
              photoObj.photos_id = vm.data[item]['id'];
              photoObj.my_users_id = identity;
              photoObj.title = JSON.stringify(vm.data[item]['title']);
              photoObj.description = JSON.stringify(vm.data[item]['description']);
              photoObj.posting_date = moment().format('DD-MM-YYYY hh:mm:ss');
              photoObj.creation_date = moment(vm.data[item]['creationDate']).format('DD-MM-YYYY hh:mm:ss');
              vm.data[item]['creationDate'] = photoObj.creation_date;
              photoObj.file_type = 'jpg';
              photoObj.asset_path = vm.data[item]['link'];
              photoObj.preview_src_link = vm.data[item]['urlImageIcon'];

              photoArray.push(photoObj);
              var tagArray = [];
              if (vm.data[item]['tags'] != undefined &&vm.data[item]['tags'].length>0) {

                console.log('l-am gasiit');
                for (var tag in vm.data[item]['tags']) {
                  var tagObj = {};
                  tagObj.my_users_id = identity;
                  tagObj.name = JSON.stringify(vm.data[item]['tags'][tag]);
                  tagObj.file_type = 'photos';
                  tagObj.photos_id = item['id'];
                  tagArray.push(tagObj);
                }
              }

            }
            console.log('[uploadData][photoObj]' + JSON.stringify(photoArray));
            var dataToSend = JSON.stringify(photoArray);
            var nodetmp = $resource('http://localhost:3005/users');
            nodetmp.save(function (resolve, reject) {
              console.log('[nodetmp]' + JSON.stringify(resolve));

            })
            var node = $resource('http://localhost:3005/addAssets/:assets',
              {
                'assets': '@assets'
              });
            node.save({
              'assets': dataToSend
            }, function (result) {
              console.log('a terminat de inserat asset');
              resolve()
              console.log('got res ' + JSON.stringify(result));
            }, function (error) {
              console.log('error adding assets');
              console.log(error);
              toastr.error(error.statusText, JSON.stringify(error.status));
            });


          }).then(function (resolve, reject) {
            //TODO upload data array
            console.log('am uploadat array-ul');
            //vm.data = [];
            vm.insertTags();
            $scope.$apply();

          })
        })
        }

        vm.getExistingPhotos = function (userId) {
          return new Promise(function (resolve, reject) {
            //TODO de returnat un array de photoId
            vm.existingPhotosIds.push(Number(1058188260977870));
            resolve();
          })
        }

        vm.photoExists = function (photoId) {
          return vm.existingPhotosIds.includes(Number(photoId));
        }

        vm.allTags = [];

        vm.insertTags = function () {
        AWSIdentityService.setAWSConfig(AWSIdentityService.getCredentials()).then(function(identity) {
          console.log('identity:'+identity);

          return new Promise(function (resolve, reject) {
            var promiseArray = [];
            for (var i in vm.data) {
              if (vm.data[i]['tags'] != undefined && vm.data[i]['tags'].length > 0) {
                console.log('found tags' + JSON.stringify(vm.data[i]['tags']));
                var index = i;

                var ind = 0;
                for (var tag in vm.data[i]['tags']) {
                  console.log('photo info for tag ' + JSON.stringify(vm.data[i]));
                  var tagItem = {};
                  tagItem.tag_name = String(vm.data[i]['tags'][tag]);
                  tagItem.file_type = 'photo';
                  tagItem.my_users_id = identity;
                  var tmp = i;
                  var creation_date = vm.data[tmp]['creationDate'];
                  console.log('creation date ' + creation_date);
                  console.log()
                  console.log('pushing');
                  promiseArray.push(vm.getAssetId(creation_date, 'photos', vm.allTags.length));

                  vm.allTags.push(tagItem);
                }
              }
            }

            console.log('[before promise all]');
            Promise.all(promiseArray).then(function (data) {
              console.log('[promiseArray tags]' + JSON.stringify(data));
              //tag_index asset_id
              for (var index in data) {
                var ob=parseInt(data[index].asset_id);

                vm.allTags[data[index].tag_index].asset_id = ob;
                var dataToSend=JSON.stringify(vm.allTags);
                console.log(' la index: '+index+ ' avem: '+JSON.stringify(data[index]));
                // vm.allTags[data[index].tag_index]=vm.allTags[data[index].tag_index];



              }
              var node = $resource('http://localhost:3005/tags/:tagArray',
                {
                  'tagArray': '@tagArray'
                });
              node.save({
                'tagArray': dataToSend
              }, function (result) {
                console.log('a terminat de inserat tags');
                // resolve()
                console.log('got res ' + JSON.stringify(result));
              }, function (error) {
                console.log('error adding tags');
                console.log(error);
                toastr.error(error.statusText, JSON.stringify(error.status));
              });
              console.log('all tags: '+JSON.stringify(vm.allTags));
            }, function (err) {
              console.log('[promiseArray tags]' + err);
            })

          })
        });
        }

        vm.getAssetId = function (creation_date, file_type, tag_index) {
          var obj = {};
          obj.tag_index = tag_index;
          console.log('getassetid '+tag_index);
          return new Promise(function (resolve, reject) {

            $http.get("http://localhost:3005/assetId/" + creation_date + '/' + file_type)
              .then(function (response) {
                console.log('[getAssetId] - succes' + JSON.stringify(response));
                obj.asset_id = parseInt(response.data);
                resolve(obj);
              }, function (err) {
                console.log('[getAssetId] - err' + JSON.stringify(err));
                reject(err);
              });
          })

        }

        vm.initFBAPI().then(vm.getLoginStatus);
      }
    ])
})
();
