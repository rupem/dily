/**
 * Created by RobertHerscovici on 5/17/2017.
 */
(function() {
  'use strict';

  angular
    .module('dily')
    .component('fbapi', {
      templateUrl: 'app/components/fbapi/fbapi.html',
      controller: 'FbapiController',
      controllerAs: 'vm',
      bindToController: true
    })
})();

