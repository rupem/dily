angular.module('dily')
.constant('ASSET_DB_CONFIGURATION', {
	assetTypes : [
					{
						type: 'Photo',
						checked: false
					}, {
						type: 'Video',
						checked: false
					}, {
						type: 'Pdf',
						checked: false
					}, {
						type: 'Doc',
						checked: false
					}
				],
	attributesOfCategories : {
					"Photo": ["title", "posting_date", "creation_date", "location", "description", "file_type"],
					"Video": ["title", "posting_date", "creation_date", "location", "description", "file_type"],
					"Pdf": ["title", "posting_date", "creation_date", "location", "description", "file_type"],
					"Doc": ["title", "posting_date", "creation_date", "location", "description", "file_type"],
					"Users": ["first_name","last_name","birthday", "telephone", "email" ,"username", "hash"]
				}
});
