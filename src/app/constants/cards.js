angular.module('dily')
.constant('WELCOME_PAGE_CARDS',[{
		title: 'Persons',
		faIcon: 'fa fa-home',
		link: '/familyTree'
	},
	{
		title: 'Upload',
		faIcon: 'fa fa-cloud-upload',
		link: '/upload'
	},
	{
		title: 'Search',
		faIcon: 'fa fa-search',
		link: '/search'
	},
	{
		title: 'Import',
		faIcon: 'fa fa-database',
		link: '/import'
	}]
);