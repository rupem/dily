(function() {
  'use strict';

  angular
    .module('dily')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('root', {
        url: '',
        abstract: true,
        views: {
          'header': {
            template: '<acme-navbar></acme-navbar>'
          },
          'footer': {
            template: '<footer-component></footer-component>'
          }
        }
      })
      .state('landingPage', {
        url: '/',
        views: {
          'container@': {
            template: '<landing-page></landing-page>'
          }
        }
      })
      .state('root.home', {
        url: '/home',
        views: {
          'container@': {
            template: '<card-container></card-container>'
          }
        }
      })
      .state('root.search', {
        url: '/search',
        views: {
          'container@': {
            template: '<list></list>'
          }
        }
      })
      .state('root.upload', {
        url: '/upload',
        views: {
          'container@': {
            template: '<upload></upload>'
          }
        }
      })
      .state('root.import', {
        url: '/import',
        views: {
          'container@': {
            template: '<import></import>'
          }
        }
      })
      .state('root.familyTree', {
        url: '/familyTree',
        views: {
          'container@': {
            template: '<family-tree></family-tree>'
          }
        }
      })
      .state('root.assetsContainer', {
        url: '/assetsContainer',
        views: {
          'container@': {
            template: '<assets-container></assets-container>'
          }
        }
      })
      .state('root.assetDetails',{
        url: '/details',
        views: {
          'container@': {
            template: '<asset-details some-body="$resolve.someBody"></asset-details>'
          }
        },
        resolve: {
          someBody: function(AssetObjectService) {
            return AssetObjectService.getCurrentObject();
          }
        }
      });

    $locationProvider.html5Mode(true);
  }
})();
