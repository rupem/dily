(function() {
  'use strict';

  angular
    .module('dily')
    .controller('AWSComponentController', [
      function() {
        var vm = this;

        AWS.config.region = 'eu-west-1';
        AWS.config.credentials = new AWS.CognitoIdentityCredentials({
          IdentityPoolId: 'eu-west-1:ec56ee47-e333-461e-9ac9-cfddf0b6dd9c',
        });
      }
    ])
}());
