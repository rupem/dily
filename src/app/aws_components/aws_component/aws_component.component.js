(function() {
  'use strict';

  angular
    .module('dily')
    .component('awsComponent', {
      controller: 'AWSComponentController',
      controllerAs: 'vm',
      templateUrl: 'app/aws_components/aws_component',
      bindToController: true
    })
}());
