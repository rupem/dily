(function() {
  'use strict';

  angular
    .module('dily')
    .factory('AWSIdentityService', ['$q',
      function($q) {
        var service = {
          setAWSConfig: setAWSConfig,
          setCredentials: setCredentials,
          getCredentials: getCredentials
        };

        return service;

        function setAWSConfig(credentialSet) {
          return $q(function(resolve, reject) {
            var loginsObj = {};

            loginsObj[credentialSet.loginAuthority] = credentialSet.accessToken;

            AWS.config.region = 'eu-west-1';
            AWS.config.credentials = new AWS.CognitoIdentityCredentials({
              IdentityPoolId: 'eu-west-1:ec56ee47-e333-461e-9ac9-cfddf0b6dd9c',
              Logins: loginsObj
            });

            AWS.config.credentials.get(function(){
              var accessKeyId = AWS.config.credentials.accessKeyId;
              var secretAccessKey = AWS.config.credentials.secretAccessKey;
              var sessionToken = AWS.config.credentials.sessionToken;

              var identityId = AWS.config.credentials.identityId;

              resolve(identityId);
            });
          })
        }

        function setCredentials(credentialSet) {
          localStorage.setItem('awsCredentialSet', JSON.stringify(credentialSet));
        }

        function getCredentials() {
          return JSON.parse(localStorage.getItem('awsCredentialSet'));
        }
      }
    ])
}());
