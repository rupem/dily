(function() {
  'use strict';

  angular
    .module('dily')
    .component('landingPage', {
      templateUrl: 'app/landing_page/landing_page.html',
      controller: 'LandingPageController',
      controllerAs: 'vm',
      bindToController: true
    })
})();
