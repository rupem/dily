/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('dily')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
