(function () {
  'use strict';

  angular
    .module('dily')
    .controller('GoogleSigninController', ['$timeout', '$scope',
      function($timeout, $scope) {
        var vm = this;
        init();

        function init() {
          if (gapi.auth2) {
            vm.authInstance = gapi.auth2.getAuthInstance();
          } else {
            gapi.load('auth2', function() {
              gapi.auth2.init({
                client_id: '53044390931-oupt5pvaf3lcjgbo3b71ebg8ohbia01u.apps.googleusercontent.com',
                cookiepolicy: 'single_host_origin',
              }).then(function(googleAuth) {
                googleAuth.then(function(onInit) {
                  onInit.then(function(authInstance) {
                    $timeout(function() {
                      vm.authInstance = authInstance;
                    })
                  })
                });
              })
            });
          }
        };
      }
    ])
}());
