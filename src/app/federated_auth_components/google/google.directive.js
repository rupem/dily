(function() {
  'use strict';

  angular
    .module('dily')
    .directive('googleSignin', ['$window',
      function($window) {
        return {
          restrict: 'A',
          template: '<a class="btn btn-block btn-social btn-google btn-lg" id="google_custom" data-onsuccess="onSignIn"><i class="fa fa-google"></i> Sign in with Google</a>',
          controller: 'GoogleSigninController',
          controllerAs: 'vm',
          replace: true,
          scope: {},
          link: function(scope, ele, attrs) {
            scope.$watch('vm.authInstance', function(authInstance) {
              if(authInstance) {
                attachSigninToElementForAuthInstance(document.getElementById('google_custom'), authInstance);
              }
            });

            function attachSigninToElementForAuthInstance(element, auth2) {
              auth2.attachClickHandler(element, {},
                function(googleUser) {
                  onSignIn(googleUser);
                }, function(error) {
                  alert(JSON.stringify(error, undefined, 2));
                });
            };

            function onSignIn(googleUser) {
              var id_token = googleUser.getAuthResponse().id_token;
              //webIdentityManager.setCredentialsForGoogle(id_token);

              var profile = googleUser.getBasicProfile();

              var accessTokenObject = {
                isValid: true,
                access_token: id_token,
                authProvider: 'accounts.google.com',
                expiresAt: googleUser.getAuthResponse().expires_at,
                user: {
                  name: profile.getName(),
                  email: profile.getEmail()
                }
              }

              var accessTokenObjectStringified = JSON.stringify(accessTokenObject);
              localStorage.setItem('access_token', accessTokenObjectStringified);

              scope.$emit('authDataRetrieved');
            };
          }
        }
      }
    ]);
}());
