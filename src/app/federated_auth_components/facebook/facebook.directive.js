(function() {
  'use strict';

  angular
    .module('dily')
    .directive('facebookSignin',  ['$window', 'AWSIdentityService','$location',
      function($window, AWSIdentityService, $location) {
        return {
          restrict: 'A',
          templateUrl: 'app/federated_auth_components/facebook/facebook_signin.template.html',
          replace: true,
          scope: {},
          link: function(scope, ele, attrs) {
            (function(d, s, id){
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) {return;}
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/en_US/sdk.js";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            function statusChangeCallback(response) {
              console.log(response);
              if (response.status === 'connected') {
                var credentialSet = {
                  loginAuthority: 'graph.facebook.com',
                  accessToken: response.authResponse.accessToken
                }

                AWSIdentityService.setAWSConfig(credentialSet).then(function(identityId) {
                  AWSIdentityService.setCredentials(credentialSet);
                  console.log(identityId);
                  console.log('success');
                  $location.path('/home');
                });

                testAPI();
              }
            }

            window.fbAsyncInit = function() {
              FB.init({
                appId      : '1058188260977870',
                cookie     : true,
                xfbml      : true,
                version    : 'v2.8'
              });

              FB.AppEvents.logPageView();

              FB.getLoginStatus(function(response) {
                statusChangeCallback(response);
              });
            };

            window.fb_login = function() {
              FB.login(function(response) {
                checkLoginState();
              });
            }

            function checkLoginState() {
              FB.getLoginStatus(function(response) {
                statusChangeCallback(response);
              });
            }

            function testAPI() {
              FB.api('/me?fields=name, picture, email', function(response) {
                console.log(response);
              });
            }
          }
        }
      }
    ]);
}());
