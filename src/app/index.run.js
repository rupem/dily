(function() {
  'use strict';

  angular
    .module('dily')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
