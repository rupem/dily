(function() {
  'use strict';

  angular
    .module('dily', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'toastr','ngFileUpload', 'gapi']);
})();
